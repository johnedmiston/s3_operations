import os
import boto
from boto.s3.key import Key

aws_access_key_id = os.environ["AWS_ACCESS_KEY_ID"]
aws_secret_access_key=os.environ["AWS_ACCESS_SECRET_KEY"]
conn = boto.connect_s3(aws_access_key_id, aws_secret_access_key)

conn.get_all_buckets()
