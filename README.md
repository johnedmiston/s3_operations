
# Cloning the app and setting up environment

`git clone https://YOURUSERNAME@bitbucket.org/hingeinc/s3_operations.git`

Check all is well and git is set up as well as the app itself:

```
git remote -v
```

Then install the necessary packages into your Python 3 environment.

* Create a new virtualenv or conda environment. For Conda:

```
conda create -n s3_operations python=3.6.3
conda activate s3_operations
```

* `pip install -r requirements.txt`

## Setting environment variables and other credentials

```
export AWS_ACCESS_KEY_ID='...'
export AWS_ACCESS_SECRET_KEY='...'
```
